#pragma once
#include "math/m_api.h"
#include "math/m_mathinclude.h"
#include "some_defs.hpp"

// Waggle-curl-twist.
xrt_quat
wct_to_quat(wct_t wct);


// Inlines.
inline double
rad(double degrees)
{
	return degrees * (M_PI / 180.0);
}

inline void
clamp(float *in, float min, float max)
{
	*in = fminf(max, fmaxf(min, *in));
}

inline void
clamp_to_r(float *in, float c, float r)
{
	clamp(in, c - r, c + r);
}
