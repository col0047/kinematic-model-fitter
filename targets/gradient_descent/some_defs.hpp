#pragma once
#include "math/m_api.h"
#include "stereokit.h"
#include "stereokit_ui.h"
using namespace sk;

#include <stdio.h>

#include <stddef.h>
#include <unistd.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "math/m_space.h"
#include "math/m_vec3.h"
#include "util/u_trace_marker.h"

struct wct_t
{
	float waggle;
	float curl;
	float twist;
};

enum HandJoint21Keypoint
{
	_21_WRIST = 0,

	_21_THMB_MCP = 1,
	_21_THMB_PXM = 2,
	_21_THMB_DST = 3,
	_21_THMB_TIP = 4,

	_21_INDX_PXM = 5,
	_21_INDX_INT = 6,
	_21_INDX_DST = 7,
	_21_INDX_TIP = 8,

	_21_MIDL_PXM = 9,
	_21_MIDL_INT = 10,
	_21_MIDL_DST = 11,
	_21_MIDL_TIP = 12,

	_21_RING_PXM = 13,
	_21_RING_INT = 14,
	_21_RING_DST = 15,
	_21_RING_TIP = 16,

	_21_LITL_PXM = 17,
	_21_LITL_INT = 18,
	_21_LITL_DST = 19,
	_21_LITL_TIP = 20
};

enum HandJoint26KP
{
	_26_PALM = 0,
	_26_WRIST = 1,

	_26_THUMB_METACARPAL = 2,
	_26_THUMB_PROXIMAL = 3,
	_26_THUMB_DISTAL = 4,
	_26_THUMB_TIP = 5,

	_26_INDEX_METACARPAL = 6,
	_26_INDEX_PROXIMAL = 7,
	_26_INDEX_INTERMEDIATE = 8,
	_26_INDEX_DISTAL = 9,
	_26_INDEX_TIP = 10,

	_26_MIDDLE_METACARPAL = 11,
	_26_MIDDLE_PROXIMAL = 12,
	_26_MIDDLE_INTERMEDIATE = 13,
	_26_MIDDLE_DISTAL = 14,
	_26_MIDDLE_TIP = 15,

	_26_RING_METACARPAL = 16,
	_26_RING_PROXIMAL = 17,
	_26_RING_INTERMEDIATE = 18,
	_26_RING_DISTAL = 19,
	_26_RING_TIP = 20,

	_26_LITTLE_METACARPAL = 21,
	_26_LITTLE_PROXIMAL = 22,
	_26_LITTLE_INTERMEDIATE = 23,
	_26_LITTLE_DISTAL = 24,
	_26_LITTLE_TIP = 25,
};

enum HandFinger
{
	HF_INDEX = 0,
	HF_MIDDLE = 1,
	HF_RING = 2,
	HF_LITTLE = 3,
};

enum FingerBone
{
	FB_METACARPAL,
	FB_PROXIMAL,
	FB_INTERMEDIATE,
	FB_DISTAL
};

enum ThumbBone
{
	TB_METACARPAL,
	TB_PROXIMAL,
	TB_DISTAL
};

struct finger_t
{
	xrt_vec3 mcp_joint_position; // Position. To get final, multiply by hand->size first
	wct_t chain_mpid[4];         // Lock twist to 0 for FB_PROXIMAL, twist and waggle to 0 for FB_INTERMEDIATE, FB_DISTAL.
	float sizes_mpid[4];         // metacapral, proximal, intermediate, distal. Multiply by hand->size first.
};


struct thumb_t
{
	xrt_vec3 mcp_joint_position;

	wct_t mcp_joint_start_rotation; // Const - extra amount to center the beginning of chain.
	// 45 degree rotation to thumb, then another 60-ish degree rotation on Z-axis.

	wct_t chain_mpd[3];
	float sizes_mpd[3];
};

typedef struct kinematic_hand_4f
{
	// The distance from the wrist to the middle-proximal joint.
	float size;

	xrt_pose wrist_pose;

	finger_t fingers[4];

	thumb_t thumb;

} kinematic_hand_4f;

const xrt_vec3 t_jts[21] = {
    {0.0028955754823982716, -0.11502042412757874, -0.333164244890213},    {-0.027907714247703552, -0.081291668117046356, -0.33244901895523071},
    {-0.051818110048770905, -0.044659879058599472, -0.32266175746917725}, {-0.060050614178180695, -0.014894240535795689, -0.31708583235740662},
    {-0.058517672121524811, 0.0096288137137889862, -0.31425228714942932}, {-0.0099363364279270172, 0.0031512826681137085, -0.33162063360214233},
    {-0.005913720466196537, 0.040085643529891968, -0.320246160030365},    {-0.00068170763552188873, 0.056085564196109772, -0.30712231993675232},
    {0.0041875634342432022, 0.0701584666967392, -0.29778611660003662},    {0.014576137065887451, 0.00078040477819740772, -0.33114409446716309},
    {0.023498957976698875, 0.039882473647594452, -0.32429441809654236},   {0.030097091570496559, 0.0575268417596817, -0.30892011523246765},
    {0.036621902137994766, 0.072174735367298126, -0.29584214091300964},   {0.034285664558410645, -0.007620424497872591, -0.32893630862236023},
    {0.045844398438930511, 0.02473512664437294, -0.3235798180103302},     {0.052239984273910522, 0.041167091578245163, -0.31127449870109558},
    {0.055612910538911819, 0.05435536801815033, -0.30026194453239441},    {0.052001938223838806, -0.022175099700689316, -0.326206237077713},
    {0.0629924014210701, 0.0017597428523004055, -0.31802359223365784},    {0.067873537540435791, 0.015730608254671097, -0.30646619200706482},
    {0.070152498781681061, 0.028015151619911194, -0.2993230819702148},
};

typedef struct gradient_chonk
{
	ptrdiff_t offs;
	bool used;
	double curr_val;
	// float min_val;              // Iffy.
	// float max_val;              // Iffy.
	double max_single_step_size; // Probably good.
	double grad;                 // d(Loss)/d(Value)
	char name[64];
} gradient_chonk;

typedef struct optimization_state
{
	// kinematic_hand_4f *input_hand;
	// kinematic_hand_4f *output_hand;
	float start_err;
	float (*obj_func)(const xrt_vec3 kine[21], const xrt_vec3 gt[21]);
} optimization_state;

struct ui_state
{
	bool disp_gt = true;
	sk::material_t sp_material;
	sk::model_t sphere;
};

enum opt_mode
{
	INITIAL_PALM_POSE,
	EVERYTHING_ELSE
};

static int num_times_evaluated = 0;