#pragma once
#include "math/m_api.h"
#include "stereokit.h"
#include "stereokit_ui.h"
using namespace sk;

#include <stdio.h>

#include <stddef.h>
#include <unistd.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "math/m_space.h"
#include "math/m_vec3.h"
#include "some_defs.hpp"
#include "some_math.hpp"
#include "randoviz.hpp"

kinematic_hand_4f o_hand;
xrt_vec3 o_jts[21];


void
kh4f_init_thumb(kinematic_hand_4f *hand, const xrt_vec3 jts_to_start[21])
{
	thumb_t *t = &hand->thumb;
	t->mcp_joint_position = {0.33097, 0, -0.25968};
	t->mcp_joint_start_rotation.waggle = -rad(45);
	t->mcp_joint_start_rotation.twist = -rad(52);

	t->sizes_mpd[0] = m_vec3_len(t_jts[_21_THMB_MCP] - t_jts[_21_THMB_PXM]) / hand->size;
	t->sizes_mpd[1] = m_vec3_len(t_jts[_21_THMB_PXM] - t_jts[_21_THMB_DST]) / hand->size;
	t->sizes_mpd[2] = m_vec3_len(t_jts[_21_THMB_DST] - t_jts[_21_THMB_TIP]) / hand->size;

	for (int i = 0; i < 3; i++) {
		t->chain_mpd[i].curl = -rad(5);
	}
}

void
kh4f_init(kinematic_hand_4f *out_hand, const xrt_vec3 jts_to_start[21])
{
	memset(out_hand, 0, sizeof(kinematic_hand_4f));
	out_hand->wrist_pose.position = jts_to_start[_21_WRIST];
	out_hand->wrist_pose.orientation = {0, 0, 0, 1};

	xrt_vec3 plus_x = m_vec3_normalize(jts_to_start[_21_INDX_PXM] - jts_to_start[_21_LITL_PXM]);
	xrt_vec3 plus_z = m_vec3_normalize(jts_to_start[_21_WRIST] - jts_to_start[_21_MIDL_PXM]);

	// CHANGE THE ORDER for right hands!
	// math_vec3_cross(&plus_y, &plus_x, &plus_z);
	math_quat_from_plus_x_z(&plus_x, &plus_z, &out_hand->wrist_pose.orientation);


	math_quat_normalize(&out_hand->wrist_pose.orientation);
	printf("%f %f %f %f\n\n", out_hand->wrist_pose.orientation.x, out_hand->wrist_pose.orientation.y, out_hand->wrist_pose.orientation.z, out_hand->wrist_pose.orientation.w);

	out_hand->size = m_vec3_len(t_jts[_21_WRIST] - t_jts[_21_MIDL_PXM]);

	constexpr int start = _21_INDX_PXM;

	float wagg = -0.19;

	for (int finger = HF_INDEX; finger <= HF_LITTLE; finger++) {
		finger_t *of = &out_hand->fingers[finger];

		of->chain_mpid[FB_METACARPAL].waggle = wagg;
		wagg += 0.19f;

		of->chain_mpid[FB_PROXIMAL].waggle = 0;
		of->chain_mpid[FB_PROXIMAL].curl = rad(-5);
		of->chain_mpid[FB_PROXIMAL].twist = 0.0f;

		of->chain_mpid[FB_INTERMEDIATE].curl = rad(-5);
		of->chain_mpid[FB_DISTAL].curl = rad(-5);

		for (int bone = FB_PROXIMAL; bone <= FB_DISTAL; bone++) {
			int curr = start + (finger * 4) + bone;
			int prev = start + (finger * 4) + bone - 1;
			float dist = m_vec3_len(t_jts[prev] - t_jts[curr]);
			float norm_dist = dist / out_hand->size;
			sk::log_diagf("%d->%d: %5f %5f", prev, curr, dist, norm_dist);
			of->sizes_mpid[bone] = norm_dist;
		}
	}

	out_hand->fingers[HF_INDEX].sizes_mpid[FB_METACARPAL] = 0.697;
	out_hand->fingers[HF_MIDDLE].sizes_mpid[FB_METACARPAL] = 0.645;
	out_hand->fingers[HF_RING].sizes_mpid[FB_METACARPAL] = 0.58;
	out_hand->fingers[HF_LITTLE].sizes_mpid[FB_METACARPAL] = 0.52;

	out_hand->fingers[HF_INDEX].mcp_joint_position = {0.16926, 0, -0.34437};
	out_hand->fingers[HF_MIDDLE].mcp_joint_position = {0.034639, 0, -0.35573};
	out_hand->fingers[HF_RING].mcp_joint_position = {-0.063625, 0, -0.34164};
	out_hand->fingers[HF_LITTLE].mcp_joint_position = {-0.1509, 0, -0.30373};
	kh4f_init_thumb(out_hand, jts_to_start);
}

void
xsg_resolve_to_pose(xrt_space_graph *xsg, xrt_pose *out)
{
	xrt_space_relation temp;
	m_space_graph_resolve(xsg, &temp);
	*out = temp.pose;
}


// Note to self: Const, because we really really don't want to modify the `kinematic_hand_4f` we're getting.
void
_to_26p_4f(const kinematic_hand_4f *hand, xrt_pose p[26])
{
	XRT_TRACE_MARKER();
	xrt_space_graph bob = {};

	xrt_space_relation *rels[5];

	rels[4] = m_space_graph_reserve(&bob); // tip
	rels[3] = m_space_graph_reserve(&bob); // dst
	rels[2] = m_space_graph_reserve(&bob); // int
	rels[1] = m_space_graph_reserve(&bob); // prox
	rels[0] = m_space_graph_reserve(&bob); // mcp
	xrt_space_relation *rel_wrist = m_space_graph_reserve(&bob);

	xrt_space_relation ident = {};
	ident.pose.orientation.w = 1.0f;
	ident.relation_flags = (enum xrt_space_relation_flags) //
	    (XRT_SPACE_RELATION_ORIENTATION_VALID_BIT |        //
	     XRT_SPACE_RELATION_ORIENTATION_TRACKED_BIT |      //
	     XRT_SPACE_RELATION_POSITION_VALID_BIT |           //
	     XRT_SPACE_RELATION_POSITION_TRACKED_BIT);


	*rels[0] = ident;
	*rels[1] = ident;
	*rels[2] = ident;
	*rels[3] = ident;
	*rels[4] = ident;
	*rel_wrist = ident;

	rel_wrist->pose = hand->wrist_pose;

	int root = _26_INDEX_METACARPAL;

	for (int finger = 0; finger < 4; finger++) {

		const finger_t *of = &hand->fingers[finger];

		*rels[0] = ident;
		*rels[1] = ident;
		*rels[2] = ident;
		*rels[3] = ident;
		*rels[4] = ident;

		xrt_vec3 mcp_joint_position = of->mcp_joint_position;
		math_vec3_scalar_mul(hand->size, &mcp_joint_position); // Don't touch the hand's params!

		rels[0]->pose.position = mcp_joint_position;
		rels[0]->pose.orientation = wct_to_quat(of->chain_mpid[FB_METACARPAL]);
		xsg_resolve_to_pose(&bob, &p[root + finger * 5]);

		int bone = FB_PROXIMAL;

		for (; bone <= FB_DISTAL; bone++) {
			// Root joints of each bone.
			// Confusingly, we're assigning the

			// Translate by this bone
			rels[bone]->pose.position = {0, 0, -of->sizes_mpid[bone - 1] * hand->size};
			rels[bone]->pose.orientation = wct_to_quat(of->chain_mpid[bone]);
			xsg_resolve_to_pose(&bob, &p[root + finger * 5 + bone]);
		}

		// Finger-tip "joint" / end of distal bone.
		rels[FB_DISTAL + 1]->pose.orientation = XRT_QUAT_IDENTITY;
		rels[FB_DISTAL + 1]->pose.position = {0, 0, -of->sizes_mpid[FB_DISTAL] * hand->size};
		xsg_resolve_to_pose(&bob, &p[root + finger * 5 + FB_DISTAL + 1]);
	}
}

// Note to self: Const, because we really really don't want to modify the `kinematic_hand_4f` we're getting.
void
_to_26p_thumb(const kinematic_hand_4f *hand, xrt_pose p[26])
{
	XRT_TRACE_MARKER();
	xrt_space_graph bob = {};

	xrt_space_relation *rels[5];

	rels[4] = m_space_graph_reserve(&bob); // tip
	rels[3] = m_space_graph_reserve(&bob); // int
	rels[2] = m_space_graph_reserve(&bob); // prox
	rels[1] = m_space_graph_reserve(&bob); // mcp
	rels[0] = m_space_graph_reserve(&bob); // initial
	xrt_space_relation *rel_wrist = m_space_graph_reserve(&bob);

	xrt_space_relation ident = {};
	ident.pose.orientation.w = 1.0f;
	ident.relation_flags = (enum xrt_space_relation_flags) //
	    (XRT_SPACE_RELATION_ORIENTATION_VALID_BIT |        //
	     XRT_SPACE_RELATION_ORIENTATION_TRACKED_BIT |      //
	     XRT_SPACE_RELATION_POSITION_VALID_BIT |           //
	     XRT_SPACE_RELATION_POSITION_TRACKED_BIT);


	*rels[0] = ident;
	*rels[1] = ident;
	*rels[2] = ident;
	*rels[3] = ident;
	*rels[4] = ident;
	*rel_wrist = ident;

	rel_wrist->pose = hand->wrist_pose;


	const thumb_t *of = &hand->thumb;



	xrt_vec3 mcp_joint_position = of->mcp_joint_position;
	math_vec3_scalar_mul(hand->size, &mcp_joint_position); // Don't touch the hand's params!

	rels[0]->pose.position = mcp_joint_position;
	rels[0]->pose.orientation = wct_to_quat(of->mcp_joint_start_rotation);

	rels[1]->pose.position = {0, 0, 0};
	rels[1]->pose.orientation = wct_to_quat(of->chain_mpd[TB_METACARPAL]);
	xsg_resolve_to_pose(&bob, &p[_26_THUMB_METACARPAL]);

	rels[2]->pose.position = {0, 0, -of->sizes_mpd[TB_METACARPAL] * hand->size};
	rels[2]->pose.orientation = wct_to_quat(of->chain_mpd[TB_PROXIMAL]);
	xsg_resolve_to_pose(&bob, &p[_26_THUMB_PROXIMAL]);

	rels[3]->pose.position = {0, 0, -of->sizes_mpd[TB_PROXIMAL] * hand->size};
	rels[3]->pose.orientation = wct_to_quat(of->chain_mpd[TB_DISTAL]);
	xsg_resolve_to_pose(&bob, &p[_26_THUMB_DISTAL]);

	rels[4]->pose.position = {0, 0, -of->sizes_mpd[TB_DISTAL] * hand->size};
	rels[4]->pose.orientation = XRT_QUAT_IDENTITY;
	xsg_resolve_to_pose(&bob, &p[_26_THUMB_TIP]);
}

void
kh4f_to_26p(const kinematic_hand_4f *hand, xrt_pose p[26])
{
	XRT_TRACE_MARKER();
	num_times_evaluated++;
	_to_26p_4f(hand, p);
	_to_26p_thumb(hand, p);
	p[_26_WRIST] = hand->wrist_pose;
	p[_26_PALM].orientation = p[_26_MIDDLE_METACARPAL].orientation;
	p[_26_PALM].position = p[_26_MIDDLE_METACARPAL].position + p[_26_MIDDLE_PROXIMAL].position;
	math_vec3_scalar_mul(0.5f, &p[_26_PALM].position);
}

void
kh4f_disp_to_user(const kinematic_hand_4f *hand)
{
	xrt_pose p[26] = {};
	kh4f_to_26p(hand, p);
	for (int i = 0; i < 26; i++) {
		pose_t pose;
		pose.orientation = *(sk::quat *)&p[i].orientation;
		pose.position = *(sk::vec3 *)&p[i].position;
		draw_axis(pose, 0.01f);
		char name[64];
		sprintf(name, "h%d", i);
		text_add_at(name, pose_matrix(pose, {0.1, 0.1, 0.1}), 0, text_align_bottom_left, text_align_center, 0.01);
	}
	// printf("%f %f \n", m_vec3_len(p[_26_WRIST].position - p[_26_INDEX_METACARPAL].position) / hand->size,
	//        m_vec3_len(p[_26_INDEX_PROXIMAL].position - p[_26_INDEX_METACARPAL].position) / hand->size);
}

void
j26_to_j21(const xrt_pose in[26], xrt_vec3 out[21])
{
#define p(subject) in[subject].position
	out[_21_WRIST] = p(_26_WRIST);
	// Thumb.
	for (int i = 0; i < 4; i++) {
		out[_21_THMB_MCP + i] = p(_26_THUMB_METACARPAL + i);
	}
	// Pointer.
	for (int i = 0; i < 4; i++) {
		out[_21_INDX_PXM + i] = p(_26_INDEX_PROXIMAL + i);
	}
	// Middle.
	for (int i = 0; i < 4; i++) {
		out[_21_MIDL_PXM + i] = p(_26_MIDDLE_PROXIMAL + i);
	}
	// Ring.
	for (int i = 0; i < 4; i++) {
		out[_21_RING_PXM + i] = p(_26_RING_PROXIMAL + i);
	}
	// Little.
	for (int i = 0; i < 4; i++) {
		out[_21_LITL_PXM + i] = p(_26_LITTLE_PROXIMAL + i);
	}
}

void
constrain_kinematic_hand_4f(kinematic_hand_4f *inout)
{
	XRT_TRACE_MARKER();
	// Be paranoid about orientation
	math_quat_normalize(&inout->wrist_pose.orientation);

	{ // Thumb.
		clamp_to_r(&inout->thumb.chain_mpd[TB_METACARPAL].curl, 0, rad(70));
		clamp_to_r(&inout->thumb.chain_mpd[TB_METACARPAL].waggle, 0, rad(70));
		clamp_to_r(&inout->thumb.chain_mpd[TB_METACARPAL].twist, 0, rad(40));

		// Don't bother clamping the waggle and twist for these - those are already clamped to 0.
		clamp(&inout->thumb.chain_mpd[TB_PROXIMAL].curl, rad(-100), rad(50)); // Nikitha hands
		clamp(&inout->thumb.chain_mpd[TB_DISTAL].curl, rad(-100), rad(50));   // Nikitha hands
	}
	{
		finger_t *fingers = inout->fingers;

		// Metacarpal waggle and curl. Not in a loop because I wanted to finetune each finger
		clamp_to_r(&fingers[HF_INDEX].chain_mpid[FB_METACARPAL].waggle, rad(-10), rad(20));
		clamp_to_r(&fingers[HF_MIDDLE].chain_mpid[FB_METACARPAL].waggle, rad(0), rad(20));
		clamp_to_r(&fingers[HF_LITTLE].chain_mpid[FB_METACARPAL].waggle, rad(10), rad(20));
		clamp_to_r(&fingers[HF_RING].chain_mpid[FB_METACARPAL].waggle, rad(20), rad(20));

		clamp(&fingers[HF_INDEX].chain_mpid[FB_METACARPAL].curl, rad(-10), rad(5));
		clamp(&fingers[HF_MIDDLE].chain_mpid[FB_METACARPAL].curl, rad(-5), rad(5));
		clamp(&fingers[HF_LITTLE].chain_mpid[FB_METACARPAL].curl, rad(-10), rad(5));
		clamp(&fingers[HF_RING].chain_mpid[FB_METACARPAL].curl, rad(-20), rad(5));

		for (int idx_finger = 0; idx_finger < 4; idx_finger++) {
			finger_t *finger = &fingers[idx_finger];
			// Metacarpal
			clamp_to_r(&finger->chain_mpid[FB_METACARPAL].twist, 0, rad(20));

			// Proximal
			clamp_to_r(&finger->chain_mpid[FB_PROXIMAL].waggle, 0, rad(30));
			clamp(&finger->chain_mpid[FB_PROXIMAL].curl, rad(-110), rad(50));

			// Int and dist
			clamp(&finger->chain_mpid[FB_PROXIMAL].curl, rad(-110), rad(50));
			clamp(&finger->chain_mpid[FB_PROXIMAL].curl, rad(-110), rad(50));
		}
	}
}

//! We don't want to lose gradients just because we're at the limit, and doing all that clamping is slow and pointless when we're trying to go real fast.
void
constrain_kinematic_hand_4f_for_grad_calculation(kinematic_hand_4f *inout)
{
	// Be paranoid about orientation
	math_quat_normalize(&inout->wrist_pose.orientation);
}
