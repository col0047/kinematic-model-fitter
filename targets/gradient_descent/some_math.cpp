#include "some_math.hpp"
#include "math/m_api.h"
#include "xrt/xrt_defines.h"

xrt_quat
wct_to_quat(wct_t wct)
{
	xrt_vec3 waggle_axis = {0, 1, 0};
	xrt_quat just_waggle;
	math_quat_from_angle_vector(wct.waggle, &waggle_axis, &just_waggle);

	xrt_vec3 curl_axis = {1, 0, 0};
	xrt_quat just_curl;
	math_quat_from_angle_vector(wct.curl, &curl_axis, &just_curl);

	xrt_vec3 twist_axis = {0, 0, 1};
	xrt_quat just_twist;
	math_quat_from_angle_vector(wct.twist, &twist_axis, &just_twist);

  xrt_quat out = just_waggle; // Unnecessary but much easier to look at.

  math_quat_rotate(&out, &just_curl, &out);
  math_quat_rotate(&out, &just_twist, &out);
  return out;
}