#include "math/m_api.h"
#include "stereokit.h"
#include "stereokit_ui.h"
using namespace sk;

#include <stdio.h>

#include <stddef.h>
#include <unistd.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "math/m_space.h"
#include "math/m_vec3.h"
#include "some_defs.hpp"
#include "some_math.hpp"
#include "kinematic_hand.hpp"
#include "os/os_time.h"
#include "util/u_time.h"

bool go = false;
int curr_opt_mode = INITIAL_PALM_POSE;
optimization_state mgs;
ui_state uis = {};

void
render_jts(ui_state *uis, sk::vec3 *jts, sk::pose_t pose, int num = 5)
{

	hierarchy_push(pose_matrix(pose));
	for (int i = 0; i < num; i++) {
		// material_set_color(sp_material, "color", ); //color_hsv
		render_add_model(uis->sphere, matrix_trs(jts[i]), color_hsv((float)i / (float)num, 1.0f, 1.0f, .5f));
		// line_add(jts[i], jts[i]+vec3{1,1,1}, {255, 255, 0, 255}, {255, 255, 0,
		// 255}, 0.2f);
	}
	// for (int finger = 1; finger < 5; finger++) {
	// 	line_add(jts[_21_WRIST], jts[1 + finger * 4], {255, 255, 255, 255}, {255, 255, 255, 255}, 0.001f);
	// 	line_add(jts[1 + finger * 4], jts[2 + finger * 4], {255, 255, 255, 255}, {255, 255, 255, 255}, 0.001f);
	// }
	hierarchy_pop();
}

void
print_vec3(xrt_vec3 *p)
{
	sk::log_diagf("%f %f %f", p->x, p->y, p->z);
}

float
obj_func_whole_hand(const xrt_vec3 kine[21], const xrt_vec3 gt[21])
{
	float err = 0.0f;
	err += m_vec3_len_sqrd(kine[_21_WRIST] - gt[_21_WRIST]) * 2;
	int root = _21_THMB_MCP;
	for (int finger = 0; finger < 5; finger++) {
		for (int bone = 0; bone < 4; bone++) {
			float err_c = m_vec3_len_sqrd(kine[root + finger * 4 + bone] - gt[root + finger * 4 + bone]);
			switch (bone) {
			case 0: err += 1.0 * err_c; break;
			case 1: err += 0.03 * err_c; break;
			case 2: err += 0.03 * err_c; break;
			case 3: err += 3.0 * err_c; break;
			}
		}
	}
	return err * 2;
}

float
obj_func_just_palm(const xrt_vec3 kine[21], const xrt_vec3 gt[21])
{
	float err = 0.0f;
	err += m_vec3_len_sqrd(kine[_21_WRIST] - gt[_21_WRIST]);
	err += m_vec3_len_sqrd(kine[_21_INDX_PXM] - gt[_21_INDX_PXM]);
	err += m_vec3_len_sqrd(kine[_21_MIDL_PXM] - gt[_21_MIDL_PXM]);
	err += m_vec3_len_sqrd(kine[_21_RING_PXM] - gt[_21_RING_PXM]);
	err += m_vec3_len_sqrd(kine[_21_LITL_PXM] - gt[_21_LITL_PXM]);
	return err * 2; // * (21.0f / 4.0f);
}

float
constrain_and_eval_hand_for_grad(optimization_state *os, kinematic_hand_4f *input_hand)
{
	constrain_kinematic_hand_4f_for_grad_calculation(input_hand);
	xrt_pose new_kine_poses[26];
	xrt_vec3 new_kine_locs[21];
	kh4f_to_26p(input_hand, new_kine_poses);
	j26_to_j21(new_kine_poses, new_kine_locs);

	return os->obj_func(new_kine_locs, t_jts);
}

float
constrain_and_eval_hand(optimization_state *os, kinematic_hand_4f *input_hand)
{
	constrain_kinematic_hand_4f(input_hand);
	xrt_pose new_kine_poses[26];
	xrt_vec3 new_kine_locs[21];
	kh4f_to_26p(input_hand, new_kine_poses);
	j26_to_j21(new_kine_poses, new_kine_locs);

	return os->obj_func(new_kine_locs, t_jts);
}

void
chonky_calculate_gradient(optimization_state *os, kinematic_hand_4f *input_hand, float *input_param, gradient_chonk *param_as_chonk, float max_step)
{
	kinematic_hand_4f orig = *input_hand;
	float amt = 0.00001f;

	*input_param += amt;
	// constrain_kinematic_hand_4f(input_hand);
	// xrt_vec3 new_kine_jts[21];
	// render_kinematic_hand_4f(input_hand, new_kine_jts);

	// float end_err = os->obj_func(new_kine_jts, t_jts);

	float end_err = constrain_and_eval_hand_for_grad(os, input_hand);

	float grad = (end_err - mgs.start_err) / amt;

	*input_hand = orig;

	param_as_chonk->used = true;
	param_as_chonk->curr_val = *input_param;
	param_as_chonk->max_single_step_size = fabsf(max_step / grad);
	printf("grad %f max_step %f\n", grad, param_as_chonk->max_single_step_size);
	// if (param_as_chonk->max_single_step_size < 0.1f) {
	// 	sk::log_errf("Too small max_step!");
	// }
	param_as_chonk->grad = grad;
	param_as_chonk->offs = (ptrdiff_t)input_param - (ptrdiff_t)input_hand;
	strcpy(param_as_chonk->name, "normal");
}

void
mini_step_guy(optimization_state *os, kinematic_hand_4f *input_hand, float *input_param, float max_step)
{
	kinematic_hand_4f orig = *input_hand;
	float amt = 0.00001f;

	// constrain_kinematic_hand_4f(input_hand);
	// xrt_vec3 new_kine_jts[21];
	// render_kinematic_hand_4f(input_hand, new_kine_jts);

	// float end_err = os->obj_func(new_kine_jts, t_jts);

	float start_err = constrain_and_eval_hand(os, input_hand);

	*input_param += amt;

	float end_err = constrain_and_eval_hand(os, input_hand);

	float grad = (end_err - start_err) / amt;

	*input_hand = orig;

	*input_param -= 2.0 * max_step * grad;
}

void
chonky_apply_gradients(gradient_chonk *chonks, int num_chonks, kinematic_hand_4f *out_hand, float step_size)
{
	for (int i = 0; i < num_chonks; i++) {
		// printf("offs is %td\n", chonks[i].offs);
		*((float *)((ptrdiff_t)out_hand + chonks[i].offs)) -= step_size * chonks[i].grad; //[chonks[i].offs]
	}
}

bool
chonky_find_apply_good_step_size(optimization_state *os, gradient_chonk *chonks, int num_chonks, kinematic_hand_4f *out_hand)
{
	kinematic_hand_4f orig = *out_hand;
	kinematic_hand_4f test = *out_hand;

	// double max_step_size = 100000000000000000000000.0f;
	double max_step_size = chonks[0].max_single_step_size;
	for (int i = 0; i < num_chonks; i++) {
		gradient_chonk chonk = chonks[i];
		float new_ = fmin(chonk.max_single_step_size, max_step_size);
		max_step_size = new_;
	}

	float exp = 0.1;
	float best_exp = NAN;
	float best_err = os->start_err;
	kinematic_hand_4f best = orig;
	bool start_with_is_better = true;

	for (int bs_it = 0; bs_it < 10; bs_it++) {
		test = *out_hand;
		float exp_now = pow(exp, bs_it);
		chonky_apply_gradients(chonks, num_chonks, &test, max_step_size * exp_now);
		float err_now = constrain_and_eval_hand(os, &test);
		if (err_now < best_err) {
			best = test;
			best_exp = exp_now;
			best_err = err_now;
			start_with_is_better = false;
		}

		// float err_current = chon

		// float up =
	}
	*out_hand = best;
	sk::log_errf("%f",best_exp);
	return start_with_is_better;
}

int interations = 0;
bool done = false;

bool
optimize_once()
{
	XRT_TRACE_MARKER();
	constrain_kinematic_hand_4f(&o_hand);

	xrt_pose jt_poses[26];
	kh4f_to_26p(&o_hand, jt_poses);
	j26_to_j21(jt_poses, o_jts);

	// mgs.start_err = obj_func_whole_hand(o_jts, t_jts);
	mgs.obj_func = obj_func_whole_hand;

	const float pos_step = 0.01f;
	const float rot_step = 0.01f;
	const float curl_step = rad(10.0f);

	bool should_term = true;

	{
		gradient_chonk chonks[100] = {};
		int idx = 0;
		mgs.start_err = constrain_and_eval_hand(&mgs, &o_hand);

		if (mgs.start_err < 0.0002) {
			printf("Term conditions not met but low error!\n");
			return true;
		}



		chonky_calculate_gradient(&mgs, &o_hand, &o_hand.wrist_pose.position.x, &chonks[idx++], pos_step);
		chonky_calculate_gradient(&mgs, &o_hand, &o_hand.wrist_pose.position.y, &chonks[idx++], pos_step);
		chonky_calculate_gradient(&mgs, &o_hand, &o_hand.wrist_pose.position.z, &chonks[idx++], pos_step);

		chonky_calculate_gradient(&mgs, &o_hand, &o_hand.wrist_pose.orientation.x, &chonks[idx++], rot_step);
		chonky_calculate_gradient(&mgs, &o_hand, &o_hand.wrist_pose.orientation.y, &chonks[idx++], rot_step);
		chonky_calculate_gradient(&mgs, &o_hand, &o_hand.wrist_pose.orientation.z, &chonks[idx++], rot_step);
		chonky_calculate_gradient(&mgs, &o_hand, &o_hand.wrist_pose.orientation.w, &chonks[idx++], rot_step);

		bool blah = chonky_find_apply_good_step_size(&mgs, chonks, idx, &o_hand);
		should_term = blah && should_term;
	}

	{
		{
			gradient_chonk chonks[100] = {};
			int idx = 0;
			mgs.start_err = constrain_and_eval_hand(&mgs, &o_hand);

			chonky_calculate_gradient(&mgs, &o_hand, &o_hand.thumb.chain_mpd[TB_METACARPAL].waggle, &chonks[idx++], rad(30.0));
			chonky_calculate_gradient(&mgs, &o_hand, &o_hand.thumb.chain_mpd[TB_METACARPAL].curl, &chonks[idx++], rad(30.0));
			chonky_calculate_gradient(&mgs, &o_hand, &o_hand.thumb.chain_mpd[TB_METACARPAL].twist, &chonks[idx++], rad(30.0));
			bool blah = chonky_find_apply_good_step_size(&mgs, chonks, idx, &o_hand);
			should_term = blah && should_term;
		}
		{
			gradient_chonk chonks[100] = {};
			int idx = 0;
			mgs.start_err = constrain_and_eval_hand(&mgs, &o_hand);
			chonky_calculate_gradient(&mgs, &o_hand, &o_hand.thumb.chain_mpd[TB_PROXIMAL].curl, &chonks[idx++], rad(30.0));
			chonky_calculate_gradient(&mgs, &o_hand, &o_hand.thumb.chain_mpd[TB_DISTAL].curl, &chonks[idx++], rad(30.0));
			bool blah = chonky_find_apply_good_step_size(&mgs, chonks, idx, &o_hand);
			should_term = blah && should_term;
		}
	}

	for (int finger = 0; finger < 4; finger++) {
		finger_t *of = &o_hand.fingers[finger];
		{
			gradient_chonk chonks[100] = {};
			int idx = 0;
			mgs.start_err = constrain_and_eval_hand(&mgs, &o_hand);

			chonky_calculate_gradient(&mgs, &o_hand, &of->chain_mpid[FB_METACARPAL].waggle, &chonks[idx++], rad(10.0));
			chonky_calculate_gradient(&mgs, &o_hand, &of->chain_mpid[FB_METACARPAL].curl, &chonks[idx++], rad(10.0));
			chonky_calculate_gradient(&mgs, &o_hand, &of->chain_mpid[FB_METACARPAL].twist, &chonks[idx++], rad(20.0));

			chonky_calculate_gradient(&mgs, &o_hand, &of->chain_mpid[FB_PROXIMAL].waggle, &chonks[idx++], rad(30.0));
			bool blah = chonky_find_apply_good_step_size(&mgs, chonks, idx, &o_hand);
			should_term = blah && should_term;
		}
		{
			gradient_chonk chonks[100] = {};
			int idx = 0;
			mgs.start_err = constrain_and_eval_hand(&mgs, &o_hand);

			chonky_calculate_gradient(&mgs, &o_hand, &of->chain_mpid[FB_PROXIMAL].curl, &chonks[idx++], rad(curl_step));
			chonky_calculate_gradient(&mgs, &o_hand, &of->chain_mpid[FB_INTERMEDIATE].curl, &chonks[idx++], rad(curl_step));
			chonky_calculate_gradient(&mgs, &o_hand, &of->chain_mpid[FB_DISTAL].curl, &chonks[idx++], rad(curl_step));
			bool blah = chonky_find_apply_good_step_size(&mgs, chonks, idx, &o_hand);

			should_term = blah && should_term;
		}
	}


	// interations++;
	// assert(idx < 100);
	// printf("%d params", idx);
	// if (chonky_find_apply_good_step_size(&mgs, chonks, idx, &o_hand)) {
	// 	done = true;
	// 	return true;
	// }
	return should_term;
}

bool
optimize()
{
	// for (int i = 0; i < 40; i++) {
	uint64_t begin = os_monotonic_get_ns();
	num_times_evaluated = 0;
	while (true) {
		if (optimize_once()) {
			uint64_t end = os_monotonic_get_ns();
			printf("Ding! Finished i %d renderings and %f err\n", num_times_evaluated, mgs.start_err);

			sk::log_diagf("Took %f seconds", time_ns_to_s(end - begin));
			done = true;
			return true;
		}
	}
	// sk::log_diagf("%f", o_hand.mcp_bone_dirs[0].x);
	return false;
}



int
main()
{
	u_trace_marker_init();
	sk_settings_t settings = {};
	settings.app_name = "StereoKit C";
	settings.assets_folder = "/2/XR/sk-gradient-descent/Assets";
	settings.display_preference = display_mode_flatscreen;
	settings.overlay_app = true;
	settings.overlay_priority = 1;
	if (!sk_init(settings))
		return 1;

	render_enable_skytex(false);


	uis.sp_material = material_copy_id("default/material");

	material_set_transparency(uis.sp_material, sk::transparency_add);
	material_set_color(uis.sp_material, "color", {.4, .4, .4, 1.0f});

	uis.sphere = model_create_mesh(mesh_gen_sphere(0.013f), uis.sp_material);

	kh4f_init(&o_hand, t_jts);

	while (sk_step([]() {
		go = input_key(sk::key_space);
		sk::hierarchy_push(matrix_trs({0, 0, -.5}));



		// render_kinematic_hand_4f(&o_hand, o_jts);

		if (input_key(sk::key_f) & sk::button_state_just_active) {
			uis.disp_gt = !uis.disp_gt;
		}
		if (uis.disp_gt) {
			render_jts(&uis, (sk::vec3 *)t_jts, {{0, 0, 0}, {0, 0, 0, 1}}, 21);
		}
		// render_jts((sk::vec3 *)o_jts, {{0, 0, 0}, {0, 0, 0, 1}}, 21);

		// // print_vec3(&o_jts[15]);

		if (go && !done) {
			go = !optimize();
		}
		constrain_kinematic_hand_4f(&o_hand);
		kh4f_disp_to_user(&o_hand);

		sk::hierarchy_pop();
	})) {
		if (input_key(sk::key_q) & sk::button_state_active) {
			break;
		}
	};

	sk_shutdown();
	return 0;
}
